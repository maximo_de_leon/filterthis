package com.example.maximodeleon.filterthis;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.maximodeleon.filterthis.FilterList.FilterType;
import com.example.maximodeleon.filterthis.GPUImageFilterTools.FilterAdjuster;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import utils.CameraHelper;

@SuppressWarnings("deprecation")
public class MainActivity extends AppCompatActivity implements  OnClickListener{

    private GPUImage mGpuImage;
    private CameraHelper mCameraHelper;
    private CameraLoader mCamera;
    private GPUImageFilter mFilter;
    private FilterAdjuster mFilterAdjuster;
    private FilterList mFilters;
    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final int MEDIA_TYPE_VIDEO = 2;
    private Dialog mAlertDialog;
    private GLSurfaceView mPreview;
    private View mFiltersLayout;
    private PopupWindow mPopupMenu;



    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button_capture).setOnClickListener(this);
        findViewById(R.id.button_filter).setOnClickListener(this);
        mFiltersLayout = getLayoutInflater().inflate(R.layout.filters, null);


        mPreview = (GLSurfaceView) findViewById(R.id.effectsview);
        mGpuImage = new GPUImage(this);
        mGpuImage.setGLSurfaceView(mPreview);

        mCameraHelper = new CameraHelper(this);
        mCamera = new CameraLoader();
        mAlertDialog = new Dialog(this);


        /**/
        mAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mAlertDialog.setContentView(R.layout.filters);
        mAlertDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        mAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        initFilters();

        View cameraSwitchView = findViewById(R.id.img_switch_camera);

        if (!mCameraHelper.hasBackCamera() || !mCameraHelper.hasBackCamera()){
            cameraSwitchView.setVisibility(View.GONE);
        }

    }

    private void initFilters() {
        /*Filtros*/
        mFilters = new FilterList();
        mFilters.addFilter("Contrast", FilterType.CONTRAST);
        mFilters.addFilter("Invert", FilterType.INVERT);
        mFilters.addFilter("Hue", FilterType.HUE);
        mFilters.addFilter("Gamma", FilterType.GAMMA);
        mFilters.addFilter("Sepia", FilterType.SEPIA);
        mFilters.addFilter("Grayscale", FilterType.GRAYSCALE);

        mFilters.addFilter("Posterize", FilterType.POSTERIZE);
        mFilters.addFilter("Monochrome", FilterType.MONOCHROME);
        mFilters.addFilter("Vignette", FilterType.VIGNETTE);
        mFilters.addFilter("Sketch", FilterType.SKETCH);
        mFilters.addFilter("Toon", FilterType.SMOOTH_TOON);
        mFilters.addFilter("Distortion", FilterType.BULGE_DISTORTION);
        mFilters.addFilter("False Color", FilterType.FALSE_COLOR);

    }

    private void setupClicksOnImageViews(){

        /*Contrast*/
        TextView contrastFilter = (TextView) mFiltersLayout.findViewById(R.id.contrast_fitler);
        contrastFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(0));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

        /*Invert*/
        TextView invertFilter = (TextView) mFiltersLayout.findViewById(R.id.invert_filter);
        invertFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(1));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });


        /*Hue*/
        TextView hueFilter = (TextView) mFiltersLayout.findViewById(R.id.hue_filter);
        hueFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(2));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });
        /*Gamma*/
        TextView gammaFilter = (TextView) mFiltersLayout.findViewById(R.id.gamma_filter);
        gammaFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(3));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });



       /*Sepia*/
        TextView sepiaFilter = (TextView) mFiltersLayout.findViewById(R.id.sepia_filter);
        sepiaFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(4));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

         /*Grayscale*/
        TextView grayscleFilter = (TextView) mFiltersLayout.findViewById(R.id.grayscale_filter);
        grayscleFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(5));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

        /*Posterize*/
        TextView posterizeFilter = (TextView) mFiltersLayout.findViewById(R.id.posterize_filter);
        posterizeFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(6));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

        /*Monochrome*/
        TextView monochromeFilter = (TextView) mFiltersLayout.findViewById(R.id.monochrome_filter);
        monochromeFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(7));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

        /*Vignette*/
        TextView vignetteFilter = (TextView) mFiltersLayout.findViewById(R.id.vignette_filter);
        vignetteFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(8));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

        /*Sketch*/
        TextView sketchFilter = (TextView) mFiltersLayout.findViewById(R.id.sketch_filter);
        sketchFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(9));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });

        /*Toon*/
        TextView toonFilter = (TextView) mFiltersLayout.findViewById(R.id.toon_filter);
        toonFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(10));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });


        TextView falseColorFilter = (TextView) mFiltersLayout.findViewById(R.id.falsecolor_filter);
        falseColorFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                        mFilters.filters.get(12));
                switchFilterTo(filter);
                mPopupMenu.dismiss();
            }
        });


    }

    @Override
    protected void onPostCreate (Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupClicksOnImageViews();
        /*String path = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/";
        Bitmap bitmapImage = null;
        Uri imageUri = Uri.parse("file://"+path+"GPUImage/fruit.jpg");
        try {
             bitmapImage  =MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mGpuImage.setImage(bitmapImage);

        //for (int i=0; i < mFilters.filters.size(); i++) {
            GPUImageFilter filter = FilterList.createFilterForType(MainActivity.this,
                    mFilters.filters.get(12));
            mGpuImage.setFilter(filter);
            mGpuImage.saveToPictures("GPUImage", "fruit_" + mFilters.names.get(12) + ".jpg", null);
        //}
*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera.onResume();


    }

    @Override
    protected void onPause() {
        mCamera.onPause();
        super.onPause();
    }


    private void switchFilterTo(final GPUImageFilter filter) {
        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            mGpuImage.setFilter(mFilter);
            mFilterAdjuster = new FilterAdjuster(mFilter);
        }
    }

    private void takePicture() {
        // TODO get a size that is about the size of the screen
        Camera.Parameters params = mCamera.getmCameraInstance().getParameters();
        params.setRotation(90);
        mCamera.getmCameraInstance().setParameters(params);

        mCamera.getmCameraInstance().takePicture(null, null,
                new Camera.PictureCallback() {

                    @Override
                    public void onPictureTaken(byte[] data, final Camera camera) {
                        Log.i("antes", "comienzo");
                        final File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                        if (pictureFile == null) {
                            Log.d("ASDF",
                                    "Error creating media file, check storage permissions");
                            return;
                        }

                        try {
                            FileOutputStream fos = new FileOutputStream(pictureFile);
                            fos.write(data);
                            fos.close();
                        } catch (FileNotFoundException e) {
                            Log.d("ASDF", "File not found: " + e.getMessage());
                        } catch (IOException e) {
                            Log.d("ASDF", "Error accessing file: " + e.getMessage());
                        }

                        data = null;



                        final Bitmap bitmap = BitmapFactory.decodeFile(pictureFile.getAbsolutePath());
                        //

                        /*
                        * Correr esta parte en thread para que vaya mas rapido el asunto
                        * de guardar las imagenes.
                        * */
                        Handler deleteHandler = new Handler();
                        deleteHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mGpuImage.saveToPictures(bitmap, "GPUImage",
                                        System.currentTimeMillis() + ".jpg",
                                        new GPUImage.OnPictureSavedListener() {

                                            @Override
                                            public void onPictureSaved(final Uri
                                                                               uri) {
                                                pictureFile.delete();
                                            }
                                        });
                            }
                        });

                        try {
                            //mPreview.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
                            //mPreview.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
                            mCamera.getmCameraInstance().startPreview();
                            Log.i("fin", "fin");
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void showPopup(int resourceId) {
        LinearLayout anchor =  (LinearLayout) findViewById(resourceId);
       // View layout = getLayoutInflater().inflate(R.layout.filters, null);

     if (mPopupMenu == null) {

            mPopupMenu = new PopupWindow(mFiltersLayout, anchor.getWidth(), anchor.getHeight(), true);
            mPopupMenu.setContentView(mFiltersLayout);

            //  popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            //  popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);

            mPopupMenu.setOutsideTouchable(false);
            // popup.setFocusable(true);

            mPopupMenu.setBackgroundDrawable(new BitmapDrawable());
      }
        mPopupMenu.showAsDropDown(anchor);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.button_capture:
                if (mCamera.getmCameraInstance().getParameters().getFocusMode().equals(
                        Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    takePicture();
                } else {
                    mCamera.getmCameraInstance().autoFocus(new Camera.AutoFocusCallback() {

                        @Override
                        public void onAutoFocus(final boolean success, final Camera camera) {
                            //takePicture();
                        }
                    });
                }
                break;

            case R.id.img_switch_camera:
                mCamera.switchCamera();
                break;
            case R.id.button_filter:
                //mAlertDialog.show();
                showPopup(R.id.bar);
                break;
        }
    }

    private static File getOutputMediaFile(final int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /****************/
    class CameraLoader {

        private int mCurrentCameraId = 0;
        private Camera mCameraInstance;

        public void onResume() {
            setUpCamera(mCurrentCameraId);
        }

        public void onPause() {
            releaseCamera();
        }

        public void switchCamera() {
            releaseCamera();
            mCurrentCameraId = (mCurrentCameraId + 1) % mCameraHelper.getNumberOfCameras();
            setUpCamera(mCurrentCameraId);
        }

        private void setUpCamera(final int id) {
            mCameraInstance = getCameraInstance(id);
            Camera.Parameters parameters = mCameraInstance.getParameters();
            // TODO adjust by getting supportedPreviewSizes and then choosing
            // the best one for screen size (best fill screen)
            if (parameters.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            parameters.setPreviewSize(640, 480);
            mCameraInstance.setParameters(parameters);


            int orientation = mCameraHelper.getCameraDisplayOrientation(
                    MainActivity.this, mCurrentCameraId);
            CameraHelper.CameraInfo2 cameraInfo = new CameraHelper.CameraInfo2();
            mCameraHelper.getCameraInfo(mCurrentCameraId, cameraInfo);
            boolean flipHorizontal = cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;
            mGpuImage.setUpCamera(mCameraInstance, orientation, flipHorizontal, false);
        }

        /** A safe way to get an instance of the Camera object. */
        private Camera getCameraInstance(final int id) {
            Camera c = null;
            try {
                c = mCameraHelper.openCamera(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return c;
        }

        private void releaseCamera() {
            mCameraInstance.setPreviewCallback(null);
            mCameraInstance.release();
            mCameraInstance = null;
        }

        public Camera getmCameraInstance() {
            return mCameraInstance;
        }
    }
}