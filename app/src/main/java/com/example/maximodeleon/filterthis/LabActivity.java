package com.example.maximodeleon.filterthis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;

/**
 * Created by maximodeleon on 3/25/16.
 */
public class LabActivity extends ActionBarActivity {

    public static final String PHOTO_FILE_EXTENSION = ".png";
    public static final String PHOTO_MIME_TYPE = "image/png";

    public static final String EXTRA_PHOTO_URI = "com.example.maximodeleon.filterthis.extra.PHOTO_URI";
    public static final String EXTRA_PHOTO_DATA_PATH = "com.example.maximodeleon.filterthis.extra.PHOTO_DATA_PATH";

    private Uri mUri;
    private String mDataPath;

    @Override
    protected void onCreate(final Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        mUri = intent.getParcelableExtra(EXTRA_PHOTO_URI);
        mDataPath = intent.getStringExtra(EXTRA_PHOTO_DATA_PATH);

        final ImageView imageView = new ImageView(this);
        imageView.setImageURI(mUri);

        setContentView(imageView);
    }
}
